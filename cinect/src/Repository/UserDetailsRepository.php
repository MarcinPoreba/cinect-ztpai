<?php

namespace App\Repository;

use App\Entity\UserDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserDetails[]    findAll()
 * @method UserDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserDetails::class);
    }

    // /**
    //  * @return UserDetails[] Returns an array of UserDetails objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserDetails
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function addUserDetails(string $name, string $surname, string $nationality, \DateTime $dob, string $languages): UserDetails
    {
        $entityManager = $this->getEntityManager();

        $userDetails = new UserDetails();
        $userDetails->setName($name);
        $userDetails->setSurname($surname);
        $userDetails->setNationality($nationality);
        $userDetails->setDateOfBirth($dob);
        $userDetails->setLanguages($languages);
        $userDetails->setWatchedTimeSeries(0);
        $userDetails->setWatchedTimeMovies(0);
        try {
            $entityManager->persist($userDetails);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $userDetails;
    }

    public function changeName(string $newData, UserDetails $userDetails): UserDetails
    {
        $entityManager = $this->getEntityManager();
        $userDetails->setName($newData);
        try {
            $entityManager->persist($userDetails);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $userDetails;
    }

    public function changeSurname(string $newData, UserDetails $userDetails): UserDetails
    {
        $entityManager = $this->getEntityManager();
        $userDetails->setSurname($newData);
        try {
            $entityManager->persist($userDetails);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $userDetails;
    }

    public function changeNationality(string $newData, UserDetails $userDetails): UserDetails
    {
        $entityManager = $this->getEntityManager();
        $userDetails->setNationality($newData);
        try {
            $entityManager->persist($userDetails);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $userDetails;
    }

    public function changeLanguages(string $newData, UserDetails $userDetails): UserDetails
    {
        $entityManager = $this->getEntityManager();
        $userDetails->setLanguages($newData);
        try {
            $entityManager->persist($userDetails);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $userDetails;
    }

}
