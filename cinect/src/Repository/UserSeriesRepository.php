<?php

namespace App\Repository;

use App\Entity\Series;
use App\Entity\UserSeries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;

/**
 * @method UserSeries|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSeries|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSeries[]    findAll()
 * @method UserSeries[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSeriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserSeries::class);
    }

    public function addWatchedSeriesByUser(User $user, Series $series, float $review): UserSeries
    {
        $entityManager = $this->getEntityManager();
        $userSeries = new UserSeries();
        $userSeries->setUserId($user);
        $series->setReview($series->getReview() + $review);
        $series->setReviewCounter(($series->getReviewCounter()) + 1);
        $user->getUserDetails()->setWatchedTimeSeries($user->getUserDetails()->getWatchedTimeSeries() + $series->getLength());
        $userSeries->setSeriesId($series);
        $userSeries->setUserReview($review);
        $userSeries->setWatchedAt(new \DateTime());
        try {
            $entityManager->persist($userSeries);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $userSeries;
    }

    // /**
    //  * @return UserSeries[] Returns an array of UserSeries objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserSeries
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
