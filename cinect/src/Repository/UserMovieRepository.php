<?php

namespace App\Repository;

use App\Entity\Movie;
use App\Entity\User;
use App\Entity\UserMovie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserMovie|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserMovie|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserMovie[]    findAll()
 * @method UserMovie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserMovieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserMovie::class);
    }

    public function addWatchedMovieByUser(User $user, Movie $movie, float $review): UserMovie
    {
        $entityManager = $this->getEntityManager();
        $userMovie = new UserMovie();
        $movie->setReview($movie->getReview() + $review);
        $movie->setReviewCounter(($movie->getReviewCounter()) + 1);
        $user->getUserDetails()->setWatchedTimeMovies($user->getUserDetails()->getWatchedTimeMovies() + $movie->getLength());
        $userMovie->setUserId($user);
        $userMovie->setMovieId($movie);
        $userMovie->setUserReview($review);
        $userMovie->setWatchedAt(new \DateTime());
        try {
            $entityManager->persist($userMovie);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $userMovie;
    }

    // /**
    //  * @return UserMovie[] Returns an array of UserMovie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserMovie
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
//    /**
//     * @throws ORMException
//     */
//    public function deleteUserMovie($id): UserMovie
//    {
//        $entityManager = $this->getEntityManager();
//        $user = $entityManager->getRepository('ProjectBundle:User')->find($id);
//
////        try {
////            $entityManager->remove();
////            $entityManager->flush();
////        } catch (ORMException $e) {
////        }
//        return $toDelete;
//    }
}
