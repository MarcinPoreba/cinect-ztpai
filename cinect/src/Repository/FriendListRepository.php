<?php

namespace App\Repository;

use App\Entity\FriendList;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FriendList|null find($id, $lockMode = null, $lockVersion = null)
 * @method FriendList|null findOneBy(array $criteria, array $orderBy = null)
 * @method FriendList[]    findAll()
 * @method FriendList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FriendListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FriendList::class);
    }

    public function addFriend(User $sender, User $receiver): FriendList
    {
        $entityManager = $this->getEntityManager();
        $friendRequest = new FriendList();
        $friendRequest->setUser1($sender);
        $friendRequest->setUser2($receiver);
        $friendRequest->setIsApproved(false);
        try {
            $entityManager->persist($friendRequest);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $friendRequest;
    }

    public function approve(FriendList $toApprove): FriendList
    {
        $entityManager = $this->getEntityManager();
        $toApprove->setIsApproved(true);
        try {
            $entityManager->persist($toApprove);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $toApprove;
    }

    public function deleteRow(FriendList $toDelete): FriendList
    {
        $entityManager = $this->getEntityManager();
        try {
            $entityManager->remove($toDelete);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
        return $toDelete;
    }

    // /**
    //  * @return FriendList[] Returns an array of FriendList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FriendList
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
