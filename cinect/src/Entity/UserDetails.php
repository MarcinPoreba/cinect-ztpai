<?php

namespace App\Entity;

use App\Repository\UserDetailsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserDetailsRepository::class)
 */
class UserDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nationality;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $languages;

    /**
     * @ORM\Column(type="integer")
     */
    private $watchedTimeMovies;

    /**
     * @ORM\Column(type="integer")
     */
    private $WatchedTimeSeries;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="userDetails", cascade={"persist", "remove"})
     */
    private $userData;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getLanguages(): ?string
    {
        return $this->languages;
    }

    public function setLanguages(string $languages): self
    {
        $this->languages = $languages;

        return $this;
    }

    public function getWatchedTimeMovies(): ?int
    {
        return $this->watchedTimeMovies;
    }

    public function setWatchedTimeMovies(int $watchedTimeMovies): self
    {
        $this->watchedTimeMovies = $watchedTimeMovies;

        return $this;
    }

    public function getWatchedTimeSeries(): ?int
    {
        return $this->WatchedTimeSeries;
    }

    public function setWatchedTimeSeries(int $WatchedTimeSeries): self
    {
        $this->WatchedTimeSeries = $WatchedTimeSeries;

        return $this;
    }

    public function getUserData(): ?User
    {
        return $this->userData;
    }

    public function setUserData(User $userData): self
    {
        // set the owning side of the relation if necessary
        if ($userData->getUserDetails() !== $this) {
            $userData->setUserDetails($this);
        }

        $this->userData = $userData;

        return $this;
    }
}
