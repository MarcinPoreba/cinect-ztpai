<?php

namespace App\Entity;

use App\Repository\UserMovieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserMovieRepository::class)
 */
class UserMovie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $userReview;

    /**
     * @ORM\Column(type="datetime")
     */
    private $watchedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userMovies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity=Movie::class, inversedBy="userMovies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $movieId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserReview(): ?int
    {
        return $this->userReview;
    }

    public function setUserReview(int $userReview): self
    {
        $this->userReview = $userReview;

        return $this;
    }

    public function getWatchedAt(): ?\DateTimeInterface
    {
        return $this->watchedAt;
    }

    public function setWatchedAt(\DateTimeInterface $watchedAt): self
    {
        $this->watchedAt = $watchedAt;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getMovieId(): ?Movie
    {
        return $this->movieId;
    }

    public function setMovieId(?Movie $movieId): self
    {
        $this->movieId = $movieId;

        return $this;
    }
}
