<?php

namespace App\Entity;

use App\Repository\UserSeriesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserSeriesRepository::class)
 */
class UserSeries
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $userReview;

    /**
     * @ORM\Column(type="datetime")
     */
    private $watchedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userSeries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity=Series::class, inversedBy="userSeries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $seriesId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserReview(): ?int
    {
        return $this->userReview;
    }

    public function setUserReview(int $userReview): self
    {
        $this->userReview = $userReview;

        return $this;
    }

    public function getWatchedAt(): ?\DateTimeInterface
    {
        return $this->watchedAt;
    }

    public function setWatchedAt(\DateTimeInterface $watchedAt): self
    {
        $this->watchedAt = $watchedAt;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getSeriesId(): ?Series
    {
        return $this->seriesId;
    }

    public function setSeriesId(?Series $seriesId): self
    {
        $this->seriesId = $seriesId;

        return $this;
    }
}
