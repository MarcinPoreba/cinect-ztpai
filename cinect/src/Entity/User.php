<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isLogged;

    /**
     * @ORM\OneToOne(targetEntity=UserDetails::class, inversedBy="userData", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $userDetails;

    /**
     * @ORM\OneToMany(targetEntity=UserSeries::class, mappedBy="userId", orphanRemoval=true)
     */
    private $userSeries;

    /**
     * @ORM\OneToMany(targetEntity=UserMovie::class, mappedBy="userId", orphanRemoval=true)
     */
    private $userMovies;

    /**
     * @ORM\OneToMany(targetEntity=FriendList::class, mappedBy="user1")
     */
    private $friendLists;

    /**
     * @ORM\OneToMany(targetEntity=FriendList::class, mappedBy="user2")
     */
    private $friendLists2;

    public function __construct()
    {
        $this->userSeries = new ArrayCollection();
        $this->userMovies = new ArrayCollection();
        $this->friendLists = new ArrayCollection();
        $this->friendLists2 = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsLogged(): ?bool
    {
        return $this->isLogged;
    }

    public function setIsLogged(bool $isLogged): self
    {
        $this->isLogged = $isLogged;

        return $this;
    }

    public function getUserDetails(): ?UserDetails
    {
        return $this->userDetails;
    }

    public function setUserDetails(UserDetails $userDetails): self
    {
        $this->userDetails = $userDetails;

        return $this;
    }

    /**
     * @return Collection|UserSeries[]
     */
    public function getUserSeries(): Collection
    {
        return $this->userSeries;
    }

    public function addUserSeries(UserSeries $userSeries): self
    {
        if (!$this->userSeries->contains($userSeries)) {
            $this->userSeries[] = $userSeries;
            $userSeries->setUserId($this);
        }

        return $this;
    }

    public function removeUserSeries(UserSeries $userSeries): self
    {
        if ($this->userSeries->removeElement($userSeries)) {
            // set the owning side to null (unless already changed)
            if ($userSeries->getUserId() === $this) {
                $userSeries->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserMovie[]
     */
    public function getUserMovies(): Collection
    {
        return $this->userMovies;
    }

    public function addUserMovie(UserMovie $userMovie): self
    {
        if (!$this->userMovies->contains($userMovie)) {
            $this->userMovies[] = $userMovie;
            $userMovie->setUserId($this);
        }

        return $this;
    }

    public function removeUserMovie(UserMovie $userMovie): self
    {
        if ($this->userMovies->removeElement($userMovie)) {
            // set the owning side to null (unless already changed)
            if ($userMovie->getUserId() === $this) {
                $userMovie->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FriendList[]
     */
    public function getFriendLists(): Collection
    {
        return $this->friendLists;
    }

    public function addFriendList(FriendList $friendList): self
    {
        if (!$this->friendLists->contains($friendList)) {
            $this->friendLists[] = $friendList;
            $friendList->setUser1($this);
        }

        return $this;
    }

    public function removeFriendList(FriendList $friendList): self
    {
        if ($this->friendLists->removeElement($friendList)) {
            // set the owning side to null (unless already changed)
            if ($friendList->getUser1() === $this) {
                $friendList->setUser1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FriendList[]
     */
    public function getFriendLists2(): Collection
    {
        return $this->friendLists2;
    }

    public function addFriendLists2(FriendList $friendLists2): self
    {
        if (!$this->friendLists2->contains($friendLists2)) {
            $this->friendLists2[] = $friendLists2;
            $friendLists2->setUser2($this);
        }

        return $this;
    }

    public function removeFriendLists2(FriendList $friendLists2): self
    {
        if ($this->friendLists2->removeElement($friendLists2)) {
            // set the owning side to null (unless already changed)
            if ($friendLists2->getUser2() === $this) {
                $friendLists2->setUser2(null);
            }
        }

        return $this;
    }
}
