<?php

namespace App\Controller;

use App\Entity\Series;
use App\Entity\UserSeries;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

/**
 * @Route("/api/series")
 */
class SeriesController extends AbstractController
{
    /**
     * @Route("/add", name="add_user_series", methods={"POST"})
     */
    public function addSeriesUser(Request $request): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $userId = $params['userId'];
        $seriesId = $params['seriesId'];
        $rating = $params['rating'];

        $userSeries = $this->getDoctrine()
            ->getRepository(UserSeries::class);
        $user = $this->getDoctrine()
            ->getRepository(User::class);
        $series = $this->getDoctrine()
            ->getRepository(Series::class);

        $user = $user->findOneBy(["id" => $userId]);
        $series = $series->findOneBy(["id" => $seriesId]);
//todo check if already in watched list

//        $userMovie->findBy(array("userId" => 1));
        $userSeries = $userSeries->addWatchedSeriesByUser($user, $series, $rating);
        return new Response(json_encode('success'));
    }

    /**
     * @Route("/get/{id}", name="get_user_series", methods={"get"})
     * @param int $id
     * @return Response
     */
    public function getWatchedSeriesByUser(int $id): Response
    {
        $returnArray = array();
        $userSeries = $this->getDoctrine()
            ->getRepository(UserSeries::class);

        $userSeries = $userSeries->findBy(['userId' => $id]);

        foreach ($userSeries as $o) {
            array_push($returnArray, [
                "id" => $o->getSeriesId()->getId(),
                "title" => $o->getSeriesId()->getTitle(),
                "watchedAtDate" => $o->getWatchedAt(),
                "userRating" => $o->getUserReview()
            ]);
        }

//        $userMovie->findBy(array("userId" => 1));
        return new Response(json_encode($returnArray));
//        return new Response(json_encode($userMovie->getWatchedAt()));
    }

    /**
     * @Route("/getAll/", name="get_all_series", methods={"get"})
     * @return Response
     */
    public function getAllSeries(): Response
    {
        $returnArray = array();
        $series = $this->getDoctrine()
            ->getRepository(Series::class);
        $series = $series->findAll();

        foreach ($series as $o) {
            array_push($returnArray, [
                "id" => $o->getId(),
                "title" => $o->getTitle(),
                "creationDate" => $o->getCreatedAt(),
                "userRating" => ($o->getReviewCounter() == 0 ? 0 : $o->getReview() / $o->getReviewCounter())
            ]);
        }
        return new Response(json_encode($returnArray));
    }

}
