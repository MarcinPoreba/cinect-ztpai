<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Entity\UserMovie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

/**
 * @Route("/api/movie")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/add", name="add_user_movie")
     */
    public function addMovieUser(Request $request): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $userId = $params['userId'];
        $movieId = $params['movieId'];
        $rating = $params['rating'];

        $userMovie = $this->getDoctrine()
            ->getRepository(UserMovie::class);
        $user = $this->getDoctrine()
            ->getRepository(User::class);
        $movie = $this->getDoctrine()
            ->getRepository(Movie::class);

        $user = $user->findOneBy(["id" => $userId]);
        $movie = $movie->findOneBy(["id" => $movieId]);
////todo check if already in watched list
//
////        $userMovie->findBy(array("userId" => 1));
///
        $userMovie = $userMovie->addWatchedMovieByUser($user, $movie, $rating);

        return new Response(json_encode($userMovie));
//        return new Response(json_encode('success'));
    }

    /**
     * @Route("/get/{id}", name="get_user_movies", methods={"get"})
     * @param int $id
     * @return Response
     */
    public function getWatchedMoviesByUser(int $id): Response
    {
        $returnArray = array();
        $userMovie = $this->getDoctrine()
            ->getRepository(UserMovie::class);

        $userMovie = $userMovie->findBy(['userId' => $id]);

        foreach ($userMovie as $o) {
            array_push($returnArray, [
                "id" => $o->getMovieId()->getId(),
                "title" => $o->getMovieId()->getTitle(),
                "watchedAtDate" => $o->getWatchedAt(),
                "userRating" => $o->getUserReview()
            ]);
        }

//        $userMovie->findBy(array("userId" => 1));
        return new Response(json_encode($returnArray));
//        return new Response(json_encode($userMovie->getWatchedAt()));
    }

    /**
     * @Route("/getAll/", name="get_all_movies", methods={"get"})
     * @return Response
     */
    public function getAllMovies(): Response
    {
        $returnArray = array();
        $movie = $this->getDoctrine()
            ->getRepository(Movie::class);
        $movie = $movie->findAll();

        foreach ($movie as $o) {
            array_push($returnArray, [
                "id" => $o->getId(),
                "title" => $o->getTitle(),
                "creationDate" => $o->getCreatedAt(),
                "userRating" => ($o->getReviewCounter() == 0 ? 0 : $o->getReview() / $o->getReviewCounter())
            ]);
        }
        return new Response(json_encode($returnArray));
    }

//    /**
//     * @Route("/delete/watched", name="delete_watched", methods={"delete"})
//     * @return Response
//     */
//    public function deleteWatchedMovie(): Response
//    {
//        $userMovieId = $this->getDoctrine()
//            ->getRepository(UserMovie::class)->findOneBy(['userId' => 1, 'movieId' => 1])->getId();
//        $repository = $this->getDoctrine()
//            ->getRepository(UserMovie::class);
//        $repository->deleteUserMovie($userMovieId);
//        return new Response(json_encode($userMovie->deleteUserMovie()));
//    }
}
