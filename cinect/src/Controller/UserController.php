<?php

namespace App\Controller;

use App\Entity\FriendList;
use App\Entity\User;
use App\Entity\UserDetails;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/show/{id}", name="show")
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id' => $id]);

//        $userDetails = $this->getDoctrine()
//            ->getRepository(UserDetails::class)
//            ->findOneBy($user->getUserDetailsId());

        $response = new Response();
        $response->setContent(json_encode($user->getUserDetails()->getName()));
        return new Response(json_encode([
            "id" => $user->getUserDetails()->getId(),
            "name" => $user->getUserDetails()->getName(),
            "surname" => $user->getUserDetails()->getSurname(),
            "nationality" => $user->getUserDetails()->getNationality(),
            "dateOfBirth" => $user->getUserDetails()->getDateOfBirth(),
            "languages" => $user->getUserDetails()->getLanguages(),
            "wtm" => $user->getUserDetails()->getWatchedTimeMovies(),
            "wts" => $user->getUserDetails()->getWatchedTimeSeries()
        ]));
    }

    /**
     * @Route("/edit/name/{id}", name="edit_name")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editName(Request $request, int $id): Response
    {
//        todo without request was working
        $params = $request->getContent();
        $params = json_decode($params, true);
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id' => $id]);

        $userDetails = $this->getDoctrine()
            ->getRepository(UserDetails::class);

        $userDetails->changeName($params['data'], $user->getUserDetails());

        return new Response(json_encode('success'));
    }


    /**
     * @Route("/edit/surname/{id}", name="edit_surname")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editSurname(Request $request, int $id): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id' => $id]);

        $userDetails = $this->getDoctrine()
            ->getRepository(UserDetails::class);
        $userDetails->changeSurname($params['data'], $user->getUserDetails());

        return new Response(json_encode('success'));
    }

    /**
     * @Route("/edit/nationality/{id}", name="edit_nationality")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editNationality(Request $request, int $id): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id' => $id]);

        $userDetails = $this->getDoctrine()
            ->getRepository(UserDetails::class);

        $userDetails->changeNationality($params['data'], $user->getUserDetails());
        return new Response(json_encode('success'));
    }

    /**
     * @Route("/edit/languages/{id}", name="edit_languages")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editLanguages(Request $request, int $id): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id' => $id]);

        $userDetails = $this->getDoctrine()
            ->getRepository(UserDetails::class);

        $userDetails->changeLanguages($params['data'], $user->getUserDetails());
        return new Response(json_encode('success'));
    }

    /**
     * @Route("/get/all", name="get_all_users")
     * @return Response
     */
    public function getAllUsers(): Response
    {
        $returnArray = array();
        $user = $this->getDoctrine()->getRepository(User::class);
        $user = $user->findAll();

        foreach ($user as $o) {
            array_push($returnArray, [
                "name" => $o->getUserDetails()->getName(),
                "surname" => $o->getUserDetails()->getSurname(),
                "id" => $o->getId()
            ]);
        }

        return new Response(json_encode($returnArray));
    }

    /**
     *
     * @Route("/add/friend", name="add_friend")
     * @param Request $request
     * @return Response
     */
    public function addFriend(Request $request): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $sender = $params['sender'];
        $receiver = $params['receiver'];
        if ($sender == $receiver)
            return new Response(json_encode(3));


        $friendList = $this->getDoctrine()
            ->getRepository(FriendList::class);

        $friendListCheck = $friendList->findAll();

        foreach ($friendListCheck as $o) {
            if (($o->getUser1()->getId() == $sender and $o->getUser2()->getId() == $receiver) or ($o->getUser2()->getId() == $sender and $o->getUser1()->getId() == $receiver)) {
                return new Response(json_encode(false));
            }
        }

        $userSender = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id' => $sender]);
        $userReceiver = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id' => $receiver]);

        $friendList = $friendList->addFriend($userSender, $userReceiver);

        return new Response(json_encode($sender));
    }

    /**
     *
     * @Route("/get/friend/requests", name="get_friend_requests")
     * @param Request $request
     * @return Response
     */
    public function getUserFriendRequests(Request $request): Response
    {
        $returnArray = array();
        $params = $request->getContent();
        $params = json_decode($params, true);
        $id = $params['id'];
        $friendList = $this->getDoctrine()
            ->getRepository(FriendList::class)->findBy(['user2' => $id, 'isApproved' => false]);

        foreach ($friendList as $o) {
            array_push($returnArray, [
                "senderName" => $o->getUser1()->getUserDetails()->getName(),
                "senderSurname" => $o->getUser1()->getUserDetails()->getSurname(),
                "senderId" => $o->getUser1()->getId()
            ]);
        }

        return new Response(json_encode($returnArray));
    }

    /**
     *
     * @Route("/get/friends", name="get_user_friends")
     * @param Request $request
     * @return Response
     */
    public function getUserFriends(Request $request): Response
    {
        $returnArray = array();
        $params = $request->getContent();
        $params = json_decode($params, true);
        $id = $params['id'];
        $friendList1 = $this->getDoctrine()
            ->getRepository(FriendList::class)->findBy(['user1' => $id, 'isApproved' => true]);
        $friendList2 = $this->getDoctrine()
            ->getRepository(FriendList::class)->findBy(['user2' => $id, 'isApproved' => true]);

        foreach ($friendList1 as $o) {
            array_push($returnArray, [
                "name" => $o->getUser1()->getUserDetails()->getName(),
                "surname" => $o->getUser1()->getUserDetails()->getSurname(),
                "id" => $o->getUser1()->getId()
            ]);
        }

        foreach ($friendList2 as $o) {
            array_push($returnArray, [
                "name" => $o->getUser1()->getUserDetails()->getName(),
                "surname" => $o->getUser1()->getUserDetails()->getSurname(),
                "id" => $o->getUser1()->getId()            ]);
        }

        return new Response(json_encode($returnArray));
    }

    /**
     *
     * @Route("/delete/friend", name="delete_friend")
     * @param Request $request
     * @return Response
     */
    public function deleteFriendDelete(Request $request): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $senderId = $params['senderId'];
        $receiverId = $params['receiverId'];

        $friendListRepository = $this->getDoctrine()
            ->getRepository(FriendList::class);

        if ($friendList = $this->getDoctrine()
            ->getRepository(FriendList::class)->findOneBy(['user1' => $senderId, 'user2' => $receiverId])) {
            $friendListRepository->deleteRow($friendList);
            return new Response(json_encode('success'));
        } else if ($friendList = $this->getDoctrine()
            ->getRepository(FriendList::class)->findOneBy(['user1' => $senderId, 'user2' => $receiverId])) {
            $friendListRepository->deleteRow($friendList);
            return new Response(json_encode('success'));
        }

        return new Response(json_encode('fail'));
    }
//

    /**
     *
     * @Route("/confirm/friend", name="confirm_friend_request")
     * @param Request $request
     * @return Response
     */
    public function confirmFriendRequests(Request $request): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $senderId = $params['senderId'];
        $receiverId = $params['receiverId'];

        $friendListRepository = $this->getDoctrine()
            ->getRepository(FriendList::class);

        $friendList = $this->getDoctrine()
            ->getRepository(FriendList::class)->findOneBy(['user1' => $senderId, 'user2' => $receiverId]);

        $friendListRepository->approve($friendList);
        return new Response(json_encode('success'));
    }
}
