<?php
//
//
//namespace App\Controller;
//
//
//use App\Entity\User;
//use App\Entity\UserDetails;
//use App\Repository\UserRepository;
//use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Routing\Annotation\Route;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
//use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
//use Firebase\JWT\JWT;
//
//class SecurityController extends AbstractController
//{
////    /**
////     * @Route("/login", name="app_login", methods={"POST"})
////     */
////    public function login(UserRepository $userRepository): Response
////    {
////        $email=$this->getUser()->getUsername();
////
////        $user = $userRepository->findOneBy([
////            'email'=>$email,
////        ]);
////
////        $payload = [
////            "user" => $user->getUsername(),
////            "exp"  => (new \DateTime())->modify("+5 minutes")->getTimestamp(),
////        ];
////
////
////        $jwt = JWT::encode($payload, $this->getParameter('jwt_secret'), 'HS256');
////        return $this->json([
////            'message' => 'success!',
////            'token' => sprintf('Bearer %s', $jwt),
////            'id'=>$user->getId()
////        ]);
////    }
//
//    /**
//     * @Route("/login", name="app_login")
//     */
//    public function login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
//    {
//        $params = $request->getContent();
//        $params = json_decode($params, true);
//        $email = $params['body']['email'];
//        $password = $params['body']['password'];
//
//        $user = $userRepository->findOneBy(['email'=>$email]);
//
////        if(!$user){
////            throw $this->createNotFoundException('Użytkownik nie istnieje!');
////        }
////
////        $userDetails = $user->getUserDetails();
////
////        if($user->getEmail() != $email){
////            throw $this->createNotFoundException('Niepoprawny Email!');
////        };
////
////        if(!$encoder->isPasswordValid($user, $password)){
////            throw $this->createNotFoundException('Niepoprawne Hasło!');
////        };
//
//        if (!$user || !$encoder->isPasswordValid($user, $user->getPassword())) {
//            return new Response(json_encode($params['body']['password']));
//        }
//
////
////        $name = $userDetails->getName()     ;
////        $surname = $userDetails->getSurname();
////
////
////        $payload = [
////            'user' => $user->getUsername(),
////            'exp' => (new \DateTime())->modify('+5 days')->getTimestamp(),
////        ];
////
////        $jwt = JWT::encode($payload, $this->getParameter('jwt_secret'), 'HS256');
//
//        return new Response(json_encode('successs'));
//    }
//
//    /**
//     * @Route("/logout", name="app_logout")
//     */
//    public function logout(UserRepository $userRepository):Response
//    {
//        $username = $this->getUser()->getUsername();
//        session_destroy();
//        $response = new Response();
//        $response->setContent(json_encode('Pomyślne wylogowanie'));
//        return $response;
//    }
//
//    /**
//     * @Route("/create", name="create")
//     * @param Request $request
//     * @param UserPasswordEncoderInterface $encoder
//     * @return Response
//     * @throws \Exception
//     */
//
//    public function create(Request $request, UserPasswordEncoderInterface $encoder): Response
//    {
//        $params = $request->getContent();
//        $params = json_decode($params, true);
//
//        $user = $this->getDoctrine()
//            ->getRepository(User::class)
//            ->findOneBy(['email' => $params['email']]);
//
//
//        if ($user) {
//            return new Response(json_encode('user with entered email exists'));
//        }
//
//        $userDetails = $this->getDoctrine()
//            ->getRepository(UserDetails::class);
//
//        $user = $this->getDoctrine()
//            ->getRepository(User::class);
//
//        $userDetails = $userDetails->addUserDetails($params['name'], $params['surname'], $params['nationality'], new \DateTime($params['dateOfBirth']), $params['languages']);
//
//        $user->createUser($params['email'], $params['password'], $userDetails, $encoder);
//
//        return new Response(json_encode(true));
//    }
//
//}


namespace App\Controller;


use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\User;
use App\Entity\UserDetails;
use App\Repository\UserRepository;
use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\UserDetailsRepository;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login", methods={"POST"})
     */
    public function login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $params = $request->getContent();
        $params = json_decode($params, true);
        $user = $userRepository->findOneBy([
            'email'=>$params['email'],
        ]);
        if (!$user || !$encoder->isPasswordValid($user, $params['password'])) {
            return $this->json([
                'message' => 'email or password is wrong.',
            ]);
        }

        return $this->json([
            'message' => 'success!',
            'id' => $user->getId(),
        ]);
    }


    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     * @throws \Exception
     */

    public function create(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $params = $request->getContent();
        $params = json_decode($params, true);

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['email' => $params['email']]);


        if ($user) {
            return new Response(json_encode('user with entered email exists'));
        }

        $userDetails = $this->getDoctrine()
            ->getRepository(UserDetails::class);

        $user = $this->getDoctrine()
            ->getRepository(User::class);

        $userDetails = $userDetails->addUserDetails($params['name'], $params['surname'], $params['nationality'], new \DateTime($params['dateOfBirth']), $params['languages']);

        $user->createUser($params['email'], $params['password'], $userDetails, $encoder);

        return new Response(json_encode(true));
    }

}