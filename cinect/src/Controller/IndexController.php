<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="profile")
     */
    public function profile(): Response
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/movies", name="movies")
     */
    public function addmovies(): Response
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/series", name="series")
     */
    public function addseries(): Response
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/login_page", name="login_page")
     */
    public function loginPage(): Response
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/all_users", name="all_users")
     */
    public function allUsers(): Response
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/friend_list", name="friend_list")
     */
    public function friendList(): Response
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/show_friend/{slug}", name="show_friend")
     */
    public function showFriend(): Response
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/settings", name="settings")
     */
    public function settings(): Response
    {
        return $this->render('index/index.html.twig');
    }
}
