<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210602175649 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_movie ALTER user_review TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE user_movie ALTER user_review DROP DEFAULT');
        $this->addSql('ALTER TABLE user_series ALTER user_review TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE user_series ALTER user_review DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_movie ALTER user_review TYPE INT');
        $this->addSql('ALTER TABLE user_movie ALTER user_review DROP DEFAULT');
        $this->addSql('ALTER TABLE user_series ALTER user_review TYPE INT');
        $this->addSql('ALTER TABLE user_series ALTER user_review DROP DEFAULT');
    }
}
