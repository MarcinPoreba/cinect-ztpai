<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210524150651 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE movie_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE series_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_movie_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_series_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE movie (id INT NOT NULL, title VARCHAR(255) NOT NULL, review INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, image VARCHAR(255) DEFAULT NULL, length INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE series (id INT NOT NULL, title VARCHAR(255) NOT NULL, review INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, episodes INT NOT NULL, length INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_movie (id INT NOT NULL, user_id_id INT NOT NULL, movie_id_id INT NOT NULL, user_review INT NOT NULL, watched_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FF9C09379D86650F ON user_movie (user_id_id)');
        $this->addSql('CREATE INDEX IDX_FF9C093710684CB ON user_movie (movie_id_id)');
        $this->addSql('CREATE TABLE user_series (id INT NOT NULL, user_id_id INT NOT NULL, series_id_id INT NOT NULL, user_review INT NOT NULL, watched_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5F421A109D86650F ON user_series (user_id_id)');
        $this->addSql('CREATE INDEX IDX_5F421A10ACB7A4A ON user_series (series_id_id)');
        $this->addSql('ALTER TABLE user_movie ADD CONSTRAINT FK_FF9C09379D86650F FOREIGN KEY (user_id_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_movie ADD CONSTRAINT FK_FF9C093710684CB FOREIGN KEY (movie_id_id) REFERENCES movie (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_series ADD CONSTRAINT FK_5F421A109D86650F FOREIGN KEY (user_id_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_series ADD CONSTRAINT FK_5F421A10ACB7A4A FOREIGN KEY (series_id_id) REFERENCES series (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_movie DROP CONSTRAINT FK_FF9C093710684CB');
        $this->addSql('ALTER TABLE user_series DROP CONSTRAINT FK_5F421A10ACB7A4A');
        $this->addSql('DROP SEQUENCE movie_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE series_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_movie_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_series_id_seq CASCADE');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE series');
        $this->addSql('DROP TABLE user_movie');
        $this->addSql('DROP TABLE user_series');
    }
}
