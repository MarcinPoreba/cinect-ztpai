import React, {Component} from 'react';
import {Redirect, withRouter} from 'react-router';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import AddMovies from "./pages/AddMovies";
import AddSeries from "./pages/AddSeries";
import LoginPage from "./pages/LoginPage";
import Profile from "./pages/Profile";
import ShowFriend from "./components/ShowFriend/ShowFriend";
import FriendList from "./pages/FriendList";
import AllUsers from "./pages/AllUsers";
import Settings from "./pages/Settings";
import Cookies from "universal-cookie";

class Navigation extends Component {
    render() {
        const cookies = new Cookies();

        return (
            <div className="root">
                <Router>
                    <Switch>
                        <Route path="/login_page" extact component={LoginPage}/>
                        {typeof (cookies.get('user')) !== "undefined" ?
                            <Route path="/movies" extact component={AddMovies}/> :
                            <Redirect to="/login_page"/>}
                        {typeof (cookies.get('user')) !== "undefined" ?
                            <Route path="/series" extact component={AddSeries}/> :
                            <Redirect to="/login_page"/>}
                        {typeof (cookies.get('user')) !== "undefined" ?
                            <Route path="/all_users" extact component={AllUsers}/> :
                            <Redirect to="/login_page"/>}
                        {typeof (cookies.get('user')) !== "undefined" ?
                            <Route path="/friend_list" extact component={FriendList}/> :
                            <Redirect to="/login_page"/>}
                        {typeof (cookies.get('user')) !== "undefined" ?
                            <Route path="/show_friend/:id" extact component={ShowFriend}/> :
                            <Redirect to="/login_page"/>}
                        {typeof (cookies.get('user')) !== "undefined" ?
                            <Route path="/settings" extact component={Settings}/> :
                            <Redirect to="/login_page"/>}
                        {typeof (cookies.get('user')) !== "undefined" ?
                            <Route path="/" extact component={Profile}/> :
                            <Redirect to="/login_page"/>}

                        {/*<Route path="/movies" extact component={AddMovies}/>*/}
                        {/*<Route path="/series" extact component={AddSeries}/>*/}
                        {/*<Route path="/all_users" extact component={AllUsers}/>*/}
                        {/*<Route path="/friend_list" extact component={FriendList}/>*/}
                        {/*<Route path="/show_friend/:id" extact component={ShowFriend}/>*/}
                        {/*<Route path="/settings" extact component={Settings}/>*/}
                        {/*<Route path="/" extact component={Profile}/>*/}
                    </Switch>
                </Router>
            </div>
        )
    }
}

export default withRouter(Navigation);