import React from 'react';
import Header from "../components/Header";
import Navbar from "../components/Navbar";

const Settings = () => {
    return (
        <>
            <Header/>
            <div className='content'>
                <Navbar/>

            </div>
        </>
    );
};

export default Settings;